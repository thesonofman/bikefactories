drop function if exists sel_factory_type(id int);
create or replace function sel_factory_type(id int) returns setof factory_types as $BODY$
begin
	return query select factory_type_id, factory_type_description
	from factory_types
	where factory_type_id = id;
end;
$BODY$ language plpgsql;


drop function if exists del_bike_level(id int);
create or replace function del_bike_level(id int) returns boolean as $BODY$
begin
	if exists(select 1 from levels where level_id = id) then
		delete from levels where level_id = id;
		return true;
	else
		raise notice 'no level with id %', id;
		return false;
	end if;
end;
$BODY$ language plpgsql;



drop function if exists update_bike_level(l_id int, l_name character(80), l_price_multiplier real, l_count int);
create or replace function update_bike_level(l_id int, l_name character(80), l_price_multiplier real, l_count int) returns boolean as $BODY$
begin
	if l_id is null or l_name is null or l_price_multiplier is null or l_count is null then
		raise exception 'fields l_id, l_name, l_price_multiplier, l_count must not be null';
	end if;

	if exists(select 1 from levels where level_id = l_id) then
		update levels set 
			level_name = l_name,
			level_price_multiplier = l_price_multiplier,
			level_count = l_count
		where level_id = l_id;
		return true;
	else
		raise notice 'no level with id %', l_id;
		return false;
	end if;
end;
$BODY$ language plpgsql;


drop function if exists add_worker(w_name character(80), w_position_id int, w_employed_date date, w_factory_id int);
create or replace function add_worker(w_name character(80), w_position_id int, w_employed_date date, w_factory_id int) returns void as $BODY$
begin 
	if w_name is null or w_position_id is null or w_employed_date is null or w_factory_id is null then
		raise exception 'fields w_name, w_position_id, w_employed_date, w_factory_id must not be null';
	end if;
	
	insert into workers_data(worker_name, worker_position_id, worker_employed_date, factory_id)
	values (w_name, w_position_id, w_employed_date, w_factory_id);

end;
$BODY$ language plpgsql;

-- select * from sel_factory_type(1);
-- select * from sel_factory_type(4);

-- select del_bike_level(3);
-- select del_bike_level(100);

-- select update_bike_level(2, 'First', 0.95, 3);

-- select add_worker('name', 2, '2019-09-25', 3);
-- select add_worker(null, 2, '2019-09-25', 3);







