drop function if exists sel_factory_type(id int);
create or replace function sel_factory_type(id int) returns setof factory_types as $BODY$
/**
Функция выборки из таблицы factory_types
@param id идентификатор типа завода
@return запись из таблицы, соответствующая идентификатору
*/
begin
	return query select factory_type_id, factory_type_description
	from factory_types
	where factory_type_id = id;
end;
$BODY$ language plpgsql;


drop function if exists del_bike_level(id int);
create or replace function del_bike_level(id int) returns boolean as $BODY$
/**
Функция удаления из таблицы levels
@param id идентификатор уровня
@return boolean, успешность удаления
*/
begin
	if exists(select 1 from levels where level_id = id) then
		delete from levels where level_id = id;
		return true;
	else
		raise notice 'no level with id %', id;
		return false;
	end if;
end;
$BODY$ language plpgsql;



drop function if exists update_bike_level(in l_id int, l_name character(80), l_price_multiplier real, l_count int);

create or replace function update_bike_level(in l_id int, l_name character(80), l_price_multiplier real, l_count int) returns int as $BODY$
/**
Функция вставки и обновления таблицы bike_level
@param l_id идентификатор уровня. При значении null будет произведена вставка в таблицу. При значении,
отличном от null, будет изменена запись с идентификатором l_id. Если такая запись отсутствует, будет 
выброшено исключение
@param l_name название уровня
@param l_price_multiplier коэффициент увеличения цены
@param l_count 
@throws exception Исключение будет выброшено в случае, если хотя бы один из параметров l_name, l_price_multiplier, 
l_count будет равен нулю. Также в том случае, если параметр l_id не будет равен null и запись с таким идентификатором 
будет отсутствовать
@return void
*/
declare
	new_id int;
begin
	if l_name is null or l_price_multiplier is null or l_count is null then
		raise exception 'fields l_name, l_price_multiplier, l_count must not be null';
	end if;

	if l_id is null then
		insert into levels(level_name, level_price_multiplier, level_count)
		values (l_name, l_price_multiplier, l_count)
		returning level_id into new_id;
		return new_id;
	end if;

	if exists(select 1 from levels where level_id = l_id) then
		update levels set 
		level_name = l_name,
		level_price_multiplier = l_price_multiplier,
		level_count = l_count
		where level_id = l_id
		returning level_id into new_id;
		return new_id;
	else
		raise exception 'no entry with id %', l_id;
	end if;
end;
$BODY$ language plpgsql;

-- select * from sel_factory_type(1);
-- select * from sel_factory_type(4);

-- select del_bike_level(3);
-- select del_bike_level(100);

-- select update_bike_level(2, 'First', 0.95, 3);
