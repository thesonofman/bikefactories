drop function if exists check_status_update();

create or replace function check_status_update() returns trigger as $BODY$
/**
Функция проверки возможности вставки в таблицу status
@author Galaburda
@return NEW если вставка разрешена, или NULL, если вставка не разрешена
*/
declare
	ranked record;
	old_status_rank int;
	new_status_rank int;
	last_status_rank int;
begin
	
	select dense_rank 
		into old_status_rank 
		from (select s.status_id, dense_rank() over(order by s.status_id) from status s) as ranked 
		where status_id = OLD.status_id;
		
	select dense_rank 
		into new_status_rank
		from (select s.status_id, dense_rank() over(order by s.status_id) from status s) as ranked 
		where status_id = NEW.status_id;
	
	--check for new status null
	

	if old_status_rank + 1 != new_status_rank then
		select max(dense_rank) 
			into last_status_rank
			from (select s.status_id, dense_rank() over(order by s.status_id) from status s) as ranked;
		if not (old_status_rank = last_status_rank and new_status_rank = 1) then
			raise exception 'wrong new status';
		end if;
	end if;
	return NEW;
	
end;
$BODY$ language plpgsql;

drop trigger if exists check_status_update_trigger on orders;

/**
Триггер, запрещающий изменение таблицы status не в порядке возрастания идентификатора статуса (только 1 -> 2 -> 3 -> 1)
@author Galaburda
*/
create trigger check_status_update_trigger
	before update of status_id on orders
	for each row
	execute procedure check_status_update();

-- update orders set status_id = 1 where order_id = 2;