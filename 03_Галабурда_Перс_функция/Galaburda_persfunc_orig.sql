drop function if exists bike_fac_personal_func(factory_number int);

create or replace function bike_fac_personal_func(factory_number int) 
	returns table (factory_name character(80), detail_material character(80)) as $BODY$
/** Функция выборки без использования курсора
@author Galaburda
@param Порядковый номер выбранного завода по алфавиту
@return Список материалов, из которых изготавливаются детали на выбранном заводе
*/
begin
	return query select distinct on (d_dm) f_fn, d_dm from (
		select f.factory_name f_fn, d.detail_material d_dm, dense_rank() over (order by f.factory_name) dr
		from details d
		inner join factories f on (d.factory_id = f.factory_id)
	) dense_ranked 
	where dr = factory_number;

end;
$BODY$ language plpgsql;

-- select * from bike_fac_personal_func(2);
-- select * from bike_fac_personal_func(5);