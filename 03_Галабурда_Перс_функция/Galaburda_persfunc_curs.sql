
drop function if exists bike_fac_personal_func_cursor(factory_number int);

create or replace function bike_fac_personal_func_cursor(factory_number int) 
	returns table (factory_name character(80), detail_material character(80)) as $BODY$
/** Функция выборки без использования курсора
@author Galaburda
@param Порядковый номер выбранного завода по алфавиту
@return Список материалов, из которых изготавливаются детали на выбранном заводе
*/
declare
	cursor1 refcursor;
	target_factory_id int;
	target_factory_name character(80);
begin
	open cursor1 for select f.factory_id f_id, f.factory_name f_name from factories f order by f_id;
	fetch relative factory_number from cursor1 into target_factory_id, target_factory_name;
	return query select distinct on (d_dm) target_factory_name, d.detail_material d_dm from details d where d.factory_id = target_factory_id;

end;
$BODY$ language plpgsql;

-- select * from bike_fac_personal_func_cursor(2);
-- select * from bike_fac_personal_func_cursor(5);